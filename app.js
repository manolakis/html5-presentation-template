var express = require('express')
  , http = require('http')
  , path = require('path')
  , io = require('socket.io')
  , app = express()
  , masteruser = 'username'
  , masterpass = 'password';
 
// Authentication
var auth = express.basicAuth(masteruser, masterpass);

app.configure(function(){
  app.use(express.static(path.join(__dirname, 'public')));
  app.use('/plugin', express.static(path.join(__dirname, 'public/components/reveal.js/plugin')));
});
 
app.get('/', function(req, res){
  res.sendfile(__dirname + '/views/index.html');
});

app.get('/master', auth, function(req, res){
  res.sendfile(__dirname + '/views/master.html');
});
 
var server = http.createServer(app).listen(3000, function(){
  console.log("Express server listening on port 3000");
});

io = io.listen(server);
io.sockets.on('connection', function(socket) {
  socket.emit('message', 'Welcome to Revealer');
  socket.on("slidechanged", function(data){
    socket.broadcast.emit("slidechanged", data);
  });
});
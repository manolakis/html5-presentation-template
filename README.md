# Plantilla para crear presentaciones HTML5 con [Reveal.js](https://github.com/hakimel/reveal.js "Reveal.js")

Este proyecto es una plantilla para poder crear presentaciones HTML5 utilizando Reveal.js 

## Cómo empezar

Clona el repositorio y descarga las dependencias con npm

	npm install

Una vez tengas todas las dependencias necesarias sólo tienes que ejecutar grunt para que construya tu presentación y levante un servidor donde se pueden conectar los clientes para verla.

	grunt

Por defecto el servidor se levanta en el puerto `3000` (puedes modificarlo en el fichero app.js).

Una vez esté ejecutándose el servidor puedes entrar con un navegador a [http://localhost:3000/](http://localhost:3000/ "Cliente") para acceder a la presentación como cliente. Si lo que quieres es poder controlar la aplicación puedes conectarte a [http://localhost:3000/master/](http://localhost:3000/master/ "Maestro") donde te preguntará por un usuario y una contraseña.

Los valores por defecto (pueden modificarse en el fichero app.js) son:
* usuario: `username`
* contraseña: `password`

La configuración de la presentación maestra y la cliente se encuentra en `templates/master.js` y `templates/client.js` respectivamente.

Las diapositivas debes darlas de alta en `templates/presentation.us`.

Para más opciones de configuración y creación de slides mirar [Reveal.js](https://github.com/hakimel/reveal.js "Reveal.js")
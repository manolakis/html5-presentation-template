'use strict';

var request = require('request')
  , interfaceAddresses = require('interface-addresses')
  , addresses = interfaceAddresses();

module.exports = function (grunt) {
  var reloadPort = 35729, files;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    develop: {
      server: {
        file: 'app.js'
      }
    },
    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },
      server: {
        files: [
          'app.js'
        ],
        tasks: ['develop', 'delayed-livereload']
      },
      presentation: {
        files: [
          'templates/*.us',
          'templates/*.js'
        ],
        tasks: ['template']
      },
      js: {
        files: ['public/js/*.js'],
        options: {
          livereload: reloadPort
        }
      },
      css: {
        files: ['public/css/*.css'],
        options: {
          livereload: reloadPort
        }
      }
    },
    template: {
      index: {
        src: 'templates/presentation.us',
        dest: 'views/index.html',
        variables: function() {
          return {
            host: addresses.en0,
            script: grunt.file.read('templates/client.js')
          }
        }
      },
      master: {
        src: 'templates/presentation.us',
        dest: 'views/master.html',
        variables: function() {
          return {
            host: addresses.en0,
            script: grunt.file.read('templates/master.js')
          }
        }
      }
    }
  });

  grunt.config.requires('watch.server.files');
  files = grunt.config('watch.server.files');
  files = grunt.file.expand(files);

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','),  function (err, res) {
          var reloaded = !err && res.statusCode === 200;
          if (reloaded) {
            grunt.log.ok('Delayed live reload successful.');
          } else {
            grunt.log.error('Unable to make a delayed live reload.');
          }
          done(reloaded);
        });
    }, 500);
  });

  grunt.loadNpmTasks('grunt-develop');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-templater');

  grunt.registerTask('default', ['develop', 'template', 'watch']);
};

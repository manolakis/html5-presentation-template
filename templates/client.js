(function(host){

	var socket = io.connect(host + ':3000');

	Reveal.initialize({
		history: true,
		keyboard: false,
		controls: false,
		touch: false,

		dependencies: [
			// Speaker notes
			{ src: 'plugin/notes/notes.js', async: true, condition: function() { return !!document.body.classList; } }
		]
	});
				 
	// Move to corresponding slide/ frament on receiving 
	// slidechanged event from server
	socket.on('slidechanged', function (data) {
		Reveal.slide(data.indexh, data.indexv, data.indexf);
	});

}(host));